Rails.application.routes.draw do
  resources :categories
  resources :books do
    resources :comments
    collection do
      get 'search'
    end
  end
  resources :authors
  devise_for :users
  root 'books#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :admin do
    root 'users#index'
    resources :comments
    resources :categories
    resources :books do
      member do
        post :change_status
      end
    end
    resources :authors do
      member do
        post :change_status
      end
    end
    resources :users do
      member do
        post :change_status
      end
    end
  end
end
