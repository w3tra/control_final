class AdminController < ApplicationController
  layout 'admin'
  before_action :check_admin

  private

  def check_admin
    redirect_to root_path unless user_signed_in? && current_user.admin?
  end
end