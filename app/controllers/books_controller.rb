class BooksController < ApplicationController
  before_action :set_book, only: [:show, :edit, :update, :destroy]

  # GET /books
  # GET /books.json
  def index
    @books = Book.where(active:true).page(params[:page]).per(20)
  end

  # GET /books/1
  # GET /books/1.json
  def show
    @book = Book.find(params[:id])
    @comment = Comment.new
  end

  # GET /books/new
  def new
    @book = Book.new
  end

  # GET /books/1/edit
  def edit
  end

  def create
    @user = current_user
    @book = @user.books.new(book_params)
    if @book.save
      params[:book][:categories].each do |category|
        CategoriesBook.create(book: @book, category_id: category.to_i) unless category.blank?
      end
      params[:book][:authors].each do |author|
        AuthorsBook.create(book: @book, author_id: author.to_i) unless author.blank?
      end
      redirect_to root_path, notice: 'Created, but need to approve'
    else
      render 'books/new'
    end
  end

  # PATCH/PUT /books/1
  # PATCH/PUT /books/1.json
  def update
    respond_to do |format|
      if @book.update(book_params)
        format.html { redirect_to @book, notice: 'Book was successfully updated.' }
        format.json { render :show, status: :ok, location: @book }
      else
        format.html { render :edit }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /books/1
  # DELETE /books/1.json
  def destroy
    @book.destroy
    respond_to do |format|
      format.html { redirect_to books_url, notice: 'Book was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def search
    search = params[:search]
    @books = Book.where('title LIKE ? OR desc LIKE ?', "%#{search}%", "%#{search}%").page(params[:page]).per(20)
  end

  private

  def set_book
    @book = Book.find(params[:id])
  end

  def book_params
    params.require(:book).permit(:title, :image, :desc, :agreement)
  end

end
