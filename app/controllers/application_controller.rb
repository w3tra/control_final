class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :logout_if_inactive

  private

  def logout_if_inactive
    sign_out :user unless user_signed_in? && current_user.active?
  end
end
