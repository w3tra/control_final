class Admin::CommentsController < AdminController
  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    redirect_back(fallback_location: admin_root_path)
  end
end