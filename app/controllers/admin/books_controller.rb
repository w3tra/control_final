class Admin::BooksController < AdminController
  def index
    @books = Book.all
  end

  def change_status
    @book = Book.find(params[:id])
    @book.change_status
    redirect_back(fallback_location: root_path)
  end

  def show
    @book = Book.find(params[:id])
  end
end