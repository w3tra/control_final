class Admin::UsersController < AdminController
  def index
    @users = User.all
  end

  def change_status
    @user = User.find(params[:id])
    @user.change_status
    redirect_back(fallback_location: root_path)
  end
end