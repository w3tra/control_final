class Admin::AuthorsController < AdminController
  def index
    @authors = Author.all
  end

  def change_status
    @author = Author.find(params[:id])
    @author.change_status
    redirect_back(fallback_location: root_path)
  end
end
