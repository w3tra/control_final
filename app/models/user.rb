class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :comments
  has_many :books

  def admin?
    role == 'admin'
  end

  def customer?
    role == 'customer'
  end

  def change_status
    active? ? (update_attribute :active, false) : (update_attribute :active, true)
  end
end
