class Book < ApplicationRecord
  has_many :authors_books
  has_many :categories_books
  has_many :authors, through: :authors_books
  has_many :categories, through: :categories_books
  belongs_to :user
  has_many :comments

  validates :title, presence: true
  validates :desc, presence: true
  validates :agreement, acceptance: { accept: true }

  mount_uploader :image, BookUploader

  def rating
    array = []
    comments.each do |comment|
      array << comment.rating
    end
    (array.inject{ |sum, el| sum + el }.to_f / array.size).round(1)
  end

  def change_status
    active? ? (update_attribute :active, false) : (update_attribute :active, true)
  end
end
