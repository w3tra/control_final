class Author < ApplicationRecord
  has_many :authors_books
  has_many :books, through: :authors_books

  validates :name, presence: true

  def change_status
    active? ? (update_attribute :active, false) : (update_attribute :active, true)
  end
end
