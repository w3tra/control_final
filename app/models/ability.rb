class Ability
  include CanCan::Ability
  def initialize(user)
    user ||= User.new
    if user.admin?
      can :manage, :all
      can :comment, Book
    elsif user.customer? && !user.created_at.blank?
      can :create, Book
      can :create, Author
    end
  end
end
