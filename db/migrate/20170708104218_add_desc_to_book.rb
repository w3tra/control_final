class AddDescToBook < ActiveRecord::Migration[5.1]
  def change
    add_column :books, :desc, :text
  end
end
