class AddActiveToAuthor < ActiveRecord::Migration[5.1]
  def change
    add_column :authors, :active, :boolean, default: false
  end
end
