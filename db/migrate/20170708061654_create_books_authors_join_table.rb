class CreateBooksAuthorsJoinTable < ActiveRecord::Migration[5.1]
  def change
    create_table "authors_books", id: false do |t|
      t.integer  "author_id"
      t.integer  "book_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end
  end
end
