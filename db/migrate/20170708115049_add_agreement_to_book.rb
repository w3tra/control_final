class AddAgreementToBook < ActiveRecord::Migration[5.1]
  def change
    add_column :books, :agreement, :boolean
  end
end
