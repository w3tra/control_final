class CreateBooksCategoriessJoinTable < ActiveRecord::Migration[5.1]
  def change
    create_table "categories_books", id: false do |t|
      t.integer  "category_id"
      t.integer  "book_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end
  end
end
