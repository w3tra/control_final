# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

10.times do
  Author.create(name: Faker::Book.author, active: true)
end
@authors = Author.all

10.times do
  Category.create(title: Faker::Book.genre)
end
@categories = Category.all

10.times do |n|
  User.create(email: "mail#{n}@example.com", password: 'qweqwe')
end
@users = User.all

@users.each do |user|
  5.times do
    book = user.books.create!(title: Faker::Book.title,
                              image: File.open(Rails.root + "app/assets/images/book_sample.jpeg"),
                              active: true, desc: Faker::Hipster.paragraph)
    CategoriesBook.create!(book: book, category: @categories.sample)
    AuthorsBook.create!(book: book, author: @authors.sample)
  end
end
@books = Book.all

@books.each do |book|
  5.times do
    Comment.create(user: @users.sample, body: Faker::StarWars.wookie_sentence, rating: rand(1..5), book: book)
  end
end

admin = User.create(email: 'admin@example.com', password: 'qweqwe', role: 'admin')